#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

#setup console password for root
echo "root:password" | /usr/sbin/chpasswd
passwd -u root

#setup ssh for root
sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
sed -i 's/PermitRootLogin no/PermitRootLogin yes/' /etc/ssh/sshd_config
sed -i 's/ChallengeResponseAuthentication yes/ChallengeResponseAuthentication no/' /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
echo "AllowUsers root" >> /etc/ssh/sshd_config
mkdir -p /root/.ssh
touch /root/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsC7+5r3j+lDDOH/t2P8ITZRffErsy56nkMuTuEheess5/VqjWbbDXuICAdIrLeWTiE+qUwWLpAwHDgeiU1xr973C96wNelApy9QKJfmsj5blvvn4ou3YZ2BjLBvDSiXHZk8vHgTCpnWkuamXYloAg32SV0CyTHvprlVDjbBHx0iGXJj3HLmiuZmBzyEnfU0wzk/cmmxCotY4/BnmDR8Oc3n+eC0t3pfluM81rCJzxSpKV37+V6CzwpCh2l1IU1NSl7BnHDa5rVY7mD5LgknrNnvjTVL+ujRrILpp+ywfTuDkYFaszSPqfrUz56JG5Zka+U0OnmEEyWDmM82A1emcl joshua@lupecode.com" >> "/root/.ssh/authorized_keys"
chmod 700 /root/.ssh
chmod 600 /root/.ssh/authorized_keys
service ssh restart

#Remove the snapd
echo -e "${CYAN}Removing snapd.${NC}"
systemctl disable snapd.refresh.service
systemctl disable snapd.refresh.timer 
systemctl disable snapd.autoimport.service 
systemctl disable snapd.core-fixup.service 
systemctl disable snapd.service 
systemctl disable snapd.snap-repair.timer 
systemctl disable snapd.socket 
systemctl disable snapd.system-shutdown.service 
apt remove snapd -y
apt purge snapd ubuntu-core-launcher squashfs-tools -y
echo -e "${GREEN}Done.${NC}"

#Create a swapfile
echo -e "${CYAN}Creating a swap file.${NC}"
fallocate -l 2G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
cp /etc/fstab /etc/fstab.bak
echo "/swapfile none swap sw 0 0" | sudo tee -a /etc/fstab
echo -e "${GREEN}Done.${NC}"

#Add the new repos
echo -e "${CYAN}Updating the repositories.${NC}"
apt-get update
apt-get install -y software-properties-common rsnapshot
add-apt-repository -y ppa:certbot/certbot
add-apt-repository -y ppa:ondrej/php
apt-get update
apt-get upgrade -y
apt-get autoremove -y
echo -e "${GREEN}Done.${NC}"

#Install UFW
apt-get install -y ufw

#Setup UFW
ufw allow proto tcp from any to any port 143,993,110,995,25,465,587 comment 'mail'
ufw allow proto tcp from any to any port 80,443 comment 'website'
ufw deny proto tcp from any to any port 3306 comment 'mysql'
ufw allow proto tcp from any to any port 22 comment 'ssh'
ufw enable

#Install Apache2
echo -e "${CYAN}Installing Apache2.${NC}"
apt-get install -y apache2 apache2-bin apache2-data apache2-utils
a2dismod mpm_event
a2enmod mpm_prefork actions headers proxy proxy_http rewrite socache_shmcb ssl
service apache2 restart
echo -e "${GREEN}Done.${NC}"

#Install Certbot
echo -e "${CYAN}Installing CertBot.${NC}"
apt-get install -y apt-transport-https software-properties-common python-certbot-apache python-software-properties python-setuptools
python /usr/lib/python2.7/dist-packages/easy_install.py pip
pip install certbot-dns-digitalocean
echo -e "${GREEN}Done.${NC}"

#Install PHP
echo -e "${CYAN}Installing PHP.${NC}"
#apt-get install -y php7.0 php7.0-cgi php7.0-cli php7.0-curl php7.0-gd php7.0-gmp php7.0-json php7.0-mysql php7.0-xml libapache2-mod-php7.0 php7.0-mbstring php7.0-soap php7.0-zip php7.0-xsl php7.0-bcmath php7.0-bz2 php7.0-common php7.0-mcrypt
#apt-get install -y php7.1 php7.1-cgi php7.1-cli php7.1-curl php7.1-gd php7.1-gmp php7.1-json php7.1-mysql php7.1-xml libapache2-mod-php7.1 php7.1-mbstring php7.1-soap php7.1-zip php7.1-xsl php7.1-bcmath php7.1-bz2 php7.1-common php7.1-mcrypt
apt-get install -y php7.2 php7.2-cgi php7.2-cli php7.2-curl php7.2-gd php7.2-gmp php7.2-json php7.2-mysql php7.2-xml libapache2-mod-php7.2 php7.2-mbstring php7.2-soap php7.2-zip php7.2-xsl php7.2-bcmath php7.2-bz2 php7.2-common
echo -e "${GREEN}Done.${NC}"

#Install MySQL
echo -e "${CYAN}Installing MySQL.${NC}"
apt-get install -y mysql-server
cat >> "/etc/mysql/mysql.cnf" <<EOF
[mysqld]
collation-server = utf8mb4_general_ci
character-set-server = utf8mb4
EOF
echo -e "${GREEN}Done.${NC}"

#Install Postfix, Dovecot and OpenDKIM
echo -e "${CYAN}Installing Postfix, Dovecot and OpenDKIM${NC}"
apt-get install -y postfix postfix-mysql dovecot-core dovecot-imapd dovecot-pop3d dovecot-lmtpd dovecot-mysql opendkim opendkim-tools
echo -e "${GREEN}Done.${NC}"

#Setup the single site server
echo -e "${CYAN}Enter the domain of the new server: ${NC}"
read domain
echo -e "$domain" >> /root/domain
echo -e "$domain" >> /etc/hostname
hostname "$domain"
hostnamectl set-hostname "$domain"
mkdir "/var/www/$domain"
echo -e "${CYAN}Enter the name of the mail server database: ${NC}"
read maildatabase
echo -e "${CYAN}Enter the username for the mail server database: ${NC}"
read mailuser
echo -e "${CYAN}Enter the password for $mailuser: ${NC}"
read mailpass
echo -e "${CYAN}Enter the password for the mail box admin@$domain: ${NC}"
read adminpassword

#CertBot get the wildcard certificate
echo -e "${CYAN}Setting up Certbot${NC}"
mkdir /root/.secrets
touch /root/.secrets/certbot-digitalocean.ini
echo "dns_digitalocean_token = TOKEN_HERE" >> /root/.secrets/certbot-digitalocean.ini
certbot certonly --domain "$domain" --domain "*.$domain" --server https://acme-v02.api.letsencrypt.org/directory --dns-digitalocean --dns-digitalocean-credentials /root/.secrets/certbot-digitalocean.ini --must-staple --rsa-key-size 4096
echo -e "${GREEN}Done.${NC}"

#Setup Apache2
echo -e "${CYAN}Setting up Apache config${NC}"
cat > "/var/www/$domain/index.html" <<EOF
<!doctype html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>$domain</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
            html, body {background-color: #fff; color: #636b6f; font-family: 'Raleway', sans-serif; font-weight: 100; height: 100vh; margin: 0;}
            .full-height {height: 100vh;}
            .flex-center {align-items: center; display: flex; justify-content: center;}
            .position-ref {position: relative;}
            .top-right {position: absolute; right: 10px; top: 18px;}
            .content {text-align: center;}
            .title {font-size: 84px;}
            .links > a {color: #636b6f;padding: 0 25px;font-size: 12px;font-weight: 600;letter-spacing: .1rem;text-decoration: none;text-transform: uppercase;}
            .m-b-md {margin-bottom: 30px;}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">$domain</div>
            </div>
        </div>
    </body>
</html>

EOF
cat > "/etc/apache2/sites-available/000-default.conf" <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/$domain"
	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined
	ServerAdmin webmaster@$domain
    <Directory "/var/www/$domain">
        AllowOverride All
        allow from all
        Options +Indexes
    </Directory>
    RewriteEngine on
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
EOF
cat > "/etc/apache2/sites-available/000-default-le-ssl.conf" <<EOF
<IfModule mod_ssl.c>
    <VirtualHost *:443>
        DocumentRoot "/var/www/$domain"
        <Directory "/var/www/$domain">
            AllowOverride All
            allow from all
            Options +Indexes
        </Directory>
        SSLCertificateFile /etc/letsencrypt/live/$domain/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/$domain/privkey.pem
        Include /etc/letsencrypt/options-ssl-apache.conf
        Header always set Strict-Transport-Security "max-age=31536000"
    </VirtualHost>
</IfModule>
EOF
cat > "/etc/letsencrypt/options-ssl-apache.conf" <<EOF
SSLEngine on
SSLProtocol         all -SSLv2 -SSLv3 -TLSv1
SSLCipherSuite      ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:!DSS
SSLHonorCipherOrder on
SSLCompression      off
SSLSessionTickets   off
SSLOptions +StrictRequire
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" vhost_combined
LogFormat "%v %h %l %u %t \"%r\" %>s %b" vhost_common
EOF
cat > "/etc/apache2/mods-available/ssl.conf" <<EOF
<IfModule mod_ssl.c>
    SSLRandomSeed startup builtin
    SSLRandomSeed startup file:/dev/urandom 512
    SSLRandomSeed connect builtin
    SSLRandomSeed connect file:/dev/urandom 512
    AddType application/x-x509-ca-cert .crt
    AddType application/x-pkcs7-crl .crl
    SSLPassPhraseDialog  exec:/usr/share/apache2/ask-for-passphrase
    SSLSessionCache   shmcb:${APACHE_RUN_DIR}/ssl_scache(512000)
    SSLSessionCacheTimeout  300
    SSLCipherSuite      ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:!DSS
    SSLHonorCipherOrder on
    SSLCompression      off
    SSLSessionTickets   off
    SSLProtocol all -SSLv2 -SSLv3 -TLSv1
    SSLUseStapling      On
    SSLStaplingCache    "shmcb:/logs/ssl_stapling(32768)"
</IfModule>
EOF
echo -e "${GREEN}Done.${NC}"

#Restart Apache2
echo -e "${CYAN}Restarting Apache${NC}"
a2ensite 000-default-le-ssl
chown -R www-data:www-data /var/www
service apache2 restart
rm -rf /var/www/html
echo -e "${GREEN}Done.${NC}"

#Configure Postfix
echo -e "${CYAN}Configuring Postfix${NC}"
cat > "/etc/postfix/main.cf" <<EOF
smtpd_banner = $domain ESMTP
biff = no
append_dot_mydomain = no
readme_directory = no
smtpd_tls_cert_file = /etc/letsencrypt/live/$domain/fullchain.pem
smtpd_tls_key_file = /etc/letsencrypt/live/$domain/privkey.pem
smtpd_use_tls = yes
smtpd_tls_session_cache_database = btree:\${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:\${data_directory}/smtp_scache
smtpd_tls_mandatory_ciphers = high
smtpd_tls_mandatory_exclude_ciphers = aNULL, MD5
smtpd_tls_security_level = encrypt
smtpd_tls_mandatory_protocols = !SSLv2, !SSLv3, !TLSv1
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = $domain
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
mydestination = localhost
relayhost =
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = all
smtpd_tls_auth_only = yes
smtp_tls_security_level = may
smtpd_tls_security_level = may
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth
smtpd_sasl_auth_enable = yes
smtpd_recipient_restrictions = permit_sasl_authenticated,permit_mynetworks,reject_unauth_destination
virtual_transport = lmtp:unix:private/dovecot-lmtp
virtual_mailbox_domains = mysql:/etc/postfix/mysql-virtual-mailbox-domains.cf
virtual_mailbox_maps = mysql:/etc/postfix/mysql-virtual-mailbox-maps.cf
virtual_alias_maps = mysql:/etc/postfix/mysql-virtual-alias-maps.cf,mysql:/etc/postfix/mysql-virtual-email2email.cf
milter_protocol = 2
milter_default_action = accept
smtpd_milters = inet:localhost:12301
non_smtpd_milters = inet:localhost:12301
EOF
cat > "/etc/postfix/master.cf" <<EOF
smtp      inet  n       -       y       -       -       smtpd
submission inet n       -       y       -       -       smtpd
  -o syslog_name=postfix/submission
  -o smtpd_tls_security_level=encrypt
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_reject_unlisted_recipient=no
  -o milter_macro_daemon_name=ORIGINATING
smtps     inet  n       -       y       -       -       smtpd
  -o syslog_name=postfix/smtps
  -o smtpd_tls_wrappermode=yes
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_reject_unlisted_recipient=no
  -o milter_macro_daemon_name=ORIGINATING
pickup    unix  n       -       y       60      1       pickup
cleanup   unix  n       -       y       -       0       cleanup
qmgr      unix  n       -       n       300     1       qmgr
tlsmgr    unix  -       -       y       1000?   1       tlsmgr
rewrite   unix  -       -       y       -       -       trivial-rewrite
bounce    unix  -       -       y       -       0       bounce
defer     unix  -       -       y       -       0       bounce
trace     unix  -       -       y       -       0       bounce
verify    unix  -       -       y       -       1       verify
flush     unix  n       -       y       1000?   0       flush
proxymap  unix  -       -       n       -       -       proxymap
proxywrite unix -       -       n       -       1       proxymap
smtp      unix  -       -       y       -       -       smtp
relay     unix  -       -       y       -       -       smtp
showq     unix  n       -       y       -       -       showq
error     unix  -       -       y       -       -       error
retry     unix  -       -       y       -       -       error
discard   unix  -       -       y       -       -       discard
local     unix  -       n       n       -       -       local
virtual   unix  -       n       n       -       -       virtual
lmtp      unix  -       -       y       -       -       lmtp
anvil     unix  -       -       y       -       1       anvil
scache    unix  -       -       y       -       1       scache
maildrop  unix  -       n       n       -       -       pipe
  flags=DRhu user=vmail argv=/usr/bin/maildrop -d \${recipient}
uucp      unix  -       n       n       -       -       pipe
  flags=Fqhu user=uucp argv=uux -r -n -z -a\$sender - \$nexthop!rmail (\$recipient)
ifmail    unix  -       n       n       -       -       pipe
  flags=F user=ftn argv=/usr/lib/ifmail/ifmail -r \$nexthop (\$recipient)
bsmtp     unix  -       n       n       -       -       pipe
  flags=Fq. user=bsmtp argv=/usr/lib/bsmtp/bsmtp -t\$nexthop -f\$sender \$recipient
scalemail-backend unix	-	n	n	-	2	pipe
  flags=R user=scalemail argv=/usr/lib/scalemail/bin/scalemail-store \${nexthop} \${user} \${extension}
mailman   unix  -       n       n       -       -       pipe
  flags=FR user=list argv=/usr/lib/mailman/bin/postfix-to-mailman.py
  \${nexthop} \${user}
EOF
cat > "/etc/postfix/mysql-virtual-mailbox-domains.cf" <<EOF
user = $mailuser
password = $mailpass
hosts = 127.0.0.1
dbname = $maildatabase
query = SELECT 1 FROM virtual_domains WHERE name='%s'
EOF
cat > "/etc/postfix/mysql-virtual-alias-maps.cf" <<EOF
user = $mailuser
password = $mailpass
hosts = 127.0.0.1
dbname = $maildatabase
query = SELECT destination FROM virtual_aliases WHERE source='%s'
EOF
cat > "/etc/postfix/mysql-virtual-mailbox-maps.cf" <<EOF
user = $mailuser
password = $mailpass
hosts = 127.0.0.1
dbname = $maildatabase
query = SELECT 1 FROM virtual_users WHERE email='%s'
EOF
cat > "/etc/postfix/mysql-virtual-email2email.cf" <<EOF
user = $mailuser
password = $mailpass
hosts = 127.0.0.1
dbname = $maildatabase
query = SELECT email FROM virtual_users WHERE email='%s'
EOF
echo -e "${GREEN}Done.${NC}"

#Configure MySQL
echo -e "${CYAN}Configuring MySQL${NC}"
cat > "/root/email_mysql.sql" <<EOF
DROP DATABASE IF EXISTS $maildatabase;
CREATE DATABASE $maildatabase;
USE $maildatabase;
GRANT ALL ON $maildatabase.* TO '$mailuser'@'localhost' IDENTIFIED BY '$mailpass';
FLUSH PRIVILEGES;
CREATE TABLE \`virtual_domains\` (
  \`id\` int(11) NOT NULL auto_increment,
  \`name\` varchar(50) NOT NULL,
  PRIMARY KEY (\`id\`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE \`virtual_users\` (
  \`id\` int(11) NOT NULL auto_increment,
  \`domain_id\` int(11) NOT NULL,
  \`password\` varchar(106) NOT NULL,
  \`email\` varchar(100) NOT NULL,
  PRIMARY KEY (\`id\`),
  UNIQUE KEY \`email\` (\`email\`),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE \`virtual_aliases\` (
  \`id\` int(11) NOT NULL auto_increment,
  \`domain_id\` int(11) NOT NULL,
  \`destination_id\` int(11) NULL,
  \`source\` varchar(100) NOT NULL,
  \`destination\` varchar(100) NOT NULL,
  PRIMARY KEY (\`id\`),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE,
  FOREIGN KEY (destination_id) REFERENCES virtual_users(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO \`$maildatabase\`.\`virtual_domains\` (\`id\` ,\`name\`) VALUES ('1', '$domain');
INSERT INTO \`$maildatabase\`.\`virtual_users\` (\`id\`, \`domain_id\`, \`password\` , \`email\`) VALUES ('1', '1', ENCRYPT('$adminpassword', CONCAT('\$6$', SUBSTRING(SHA(RAND()), -16))), 'admin@$domain');
INSERT INTO \`$maildatabase\`.\`virtual_aliases\` (\`id\`, \`domain_id\`, \`destination_id\`, \`source\`, \`destination\`) VALUES ('1', '1', '1', '@$domain', 'admin@$domain');
EOF
mysql -u root -p < "/root/email_mysql.sql"
echo -e "${GREEN}Done.${NC}"

#Configure Dovecot
echo -e "${CYAN}Configuring Dovecot${NC}"
mkdir -p "/var/mail/vhosts/$domain"
groupadd -g 5000 vmail
useradd -g vmail -u 5000 vmail -d /var/mail
chown -R vmail:vmail /var/mail
cat > "/etc/dovecot/dovecot.conf" <<EOF
!include_try /usr/share/dovecot/protocols.d/*.protocol
protocols = imap pop3 lmtp
!include conf.d/*.conf
!include_try local.conf
EOF
cat > "/etc/dovecot/conf.d/10-mail.conf" <<EOF
mail_location = maildir:/var/mail/vhosts/%d/%n
namespace inbox {
  inbox = yes
}
mail_privileged_group = mail
EOF
cat > "/etc/dovecot/conf.d/10-auth.conf" <<EOF
disable_plaintext_auth = yes
auth_mechanisms = plain login
!include auth-sql.conf.ext
EOF
cat > "/etc/dovecot/conf.d/auth-sql.conf.ext" <<EOF
passdb {
  driver = sql
  args = /etc/dovecot/dovecot-sql.conf.ext
}

userdb {
  driver = static
  args = uid=vmail gid=vmail home=/var/mail/vhosts/%d/%n
}
EOF
cat > "/etc/dovecot/dovecot-sql.conf.ext" <<EOF
driver = mysql
connect = host=127.0.0.1 dbname=$maildatabase user=$mailuser password=$mailpass
default_pass_scheme = SHA512-CRYPT
password_query = SELECT email as user, password FROM virtual_users WHERE email='%u';
EOF
cat > "/etc/dovecot/conf.d/10-master.conf" <<EOF
service imap-login {
  inet_listener imap {
    port = 0
  }
  inet_listener imaps {
    port = 993
    ssl = yes
  }
}
service pop3-login {
  inet_listener pop3 {
    port = 0
  }
  inet_listener pop3s {
    port = 995
    ssl = yes
  }
}
service lmtp {
    unix_listener /var/spool/postfix/private/dovecot-lmtp {
      mode = 0600
      user = postfix
      group = postfix
    }
}
service auth {
  unix_listener /var/spool/postfix/private/auth {
    mode = 0666
    user = postfix
    group = postfix
  }
  unix_listener auth-userdb {
    mode = 0600
    user = vmail
  }
  user = dovecot
}
service auth-worker {
  user = vmail
}
EOF
cat > "/etc/dovecot/conf.d/10-ssl.conf" <<EOF
ssl = required
ssl_cert = <//etc/letsencrypt/live/$domain/fullchain.pem
ssl_key = </etc/letsencrypt/live/$domain/privkey.pem
ssl_cipher_list = ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:!DSS
ssl_protocols = !SSLv2 !SSLv3 !TLSv1
ssl_prefer_server_ciphers = yes
ssl_dh_parameters_length = 2048
EOF
chown -R vmail:dovecot /etc/dovecot
service dovecot restart
echo -e "${GREEN}Done.${NC}"

#Configure DKIM
echo -e "${CYAN}Configuring DKIM${NC}"
mkdir /etc/opendkim
mkdir /etc/opendkim/keys
mkdir /etc/opendkim/keys/$domain
cat > "/etc/opendkim.conf" <<EOF
Syslog                  yes
Domain                  *
Selector                mail
AutoRestart             Yes
Background              yes
Canonicalization        relaxed/simple
DNSTimeout              5
Mode                    sv
SignatureAlgorithm      rsa-sha256
Statistics              /var/log/dkim-filter/dkim-stats

UMask                   002
AutoRestartRate         10/1h
SyslogSuccess           Yes
LogWhy                  Yes
ExternalIgnoreList      refile:/etc/opendkim/TrustedHosts
InternalHosts           refile:/etc/opendkim/TrustedHosts
KeyTable                refile:/etc/opendkim/KeyTable
SigningTable            refile:/etc/opendkim/SigningTable
PidFile                 /var/run/opendkim/opendkim.pid
Socket                  inet:12301@localhost
EOF
cat > "/etc/default/opendkim" <<EOF
SOCKET="local:/var/run/opendkim/opendkim.sock"
SOCKET="inet:12301@localhost"
EOF
cat > "/etc/opendkim/TrustedHosts" <<EOF
127.0.0.1
localhost
$domain
*.$domain
EOF
cat > "/etc/opendkim/KeyTable" <<EOF
mail._domainkey.$domain $domain:mail:/etc/opendkim/keys/$domain/mail.private
EOF
cat > "/etc/opendkim/SigningTable" <<EOF
*@$domain mail._domainkey.$domain
EOF
opendkim-genkey -s mail -d $domain -D /etc/opendkim/keys/$domain
chown opendkim:opendkim /etc/opendkim/keys/$domain/mail.private
mkdir /var/log/dkim-filter
touch /var/log/dkim-filter/dkim-stats
chown -R opendkim:opendkim /var/log/dkim-filter
service postfix restart
service opendkim restart
echo -e "${GREEN}Done.${NC}"

echo -e "${CYAN}Installing the testssl.sh script.${NC}"
cd /root/
git clone --depth 1 https://github.com/drwetter/testssl.sh.git
cat > "/root/run-testssl.sh" <<EOF
bash testssl.sh/testssl.sh $domain:443
bash testssl.sh/testssl.sh -t smtp $domain:25
bash testssl.sh/testssl.sh -t smtp $domain:587
bash testssl.sh/testssl.sh $domain:995
bash testssl.sh/testssl.sh $domain:993
EOF
echo -e "${GREEN}Done.${NC}"

echo -e "${CYAN}To test the SSL is working, run: bash /root/run-testssl.sh${NC}"
echo -e "${CYAN}The installation script has finished.${NC}"
echo -e "${RED}PLEASE REBOOT THIS SERVER.${NC}"
echo -e "${CYAN}If you want to install the mail manager, remember to give www-data permissions for sudo du in visudo.${NC}"
echo -e "${CYAN}Remember to setup backups.${NC}"
